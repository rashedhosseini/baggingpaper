More than three times improvement in terms of Mean Absolute Error in compare to other empirical procedures.

Unlike some published papers, great cares were taken not to overfit the data and also regularize the model well enough to improve model interpretability and performance.

The parameter values of the model are provided so the result may be reproducible on other datasets.

The analysis applied on a data consisting of two different datasets, one which collected by one of the authors of this paper and the other one collected in another laboratory.


3) Holger R. Maier (Graeme C. Dandy)
**** Department of Civil and Environmental Engineering, The University of Adelaide, Adelaide, S.A. 5005 Australia
Email: hmaier@civeng.adelaide.edu.au

6) E. Jabbari (O. Talebi)
** School of Civil EngineeringIran University of Science & Technology, Tehran16846113114, IranJabbari@iust.ac.ir

4) H. Md. Azmathullah 
*** Research Assistant, Central Water and Power Research Station,Khadakwasla, Pune 411 024, India; formerly, Research Scholar, CivilEngineering Dept., I.I.T, Bombay, India.
E-mail: rsazmat@civil.iitb.ac.in

* M. C. Deo
Professor, Dept. of Civil Engineering, Indian Institute of Technology,Bombay, Mumbai 400 076, India corresponding author.
E-mail: mcdeo@civil.iitb.ac.in

P. B. Deolalikar
Joint Director, Central Water and Power Research Station,Khadakwasla, Pune 411 024, India. 
E-mail: cwprs3@vsnl.net

1) Asaad Y. Shamseldin***** Department of Civil and EnvironmentalEngineering,University of Auckland,Private Bag 92019, Auckland,New ZealandE-mail: a.shamseldin@auckland.ac.nz

5) A. El-shafie (M. Mukhlisin, Ali A. Najah, M. R. Taha)
****** Department of Civil and Structural Engineering, University Kebangsaan Malaysia, Malaysia 
E-mail: elshafie@vlsi.eng.ukm.my


2) Aytac Guven
Doctor, Dept. of Civil Engineering, Univ. of Gaziantep, 27310Gaziantep, Turkey corresponding author. 
E-mail: aguven@gantep.edu.tr